/// <reference types="cypress" />

describe('Teste Geral', () => {
  it('Acessa a Pagina', () => {
    cy.visit('http://automationpractice.com/index.php');
    cy.get('.login').click()

});

  it('Faz Login no sistema', () => {
    cy.get('#email').type('zoslley@pactosolucoes.com.br');
    cy.get('#passwd').type('123456');
    cy.get('#SubmitLogin > span').click();
    
})    

  it('Adiciona um produto ao carrinho', () => {
    cy.get('li > .btn > span').click();//adiciona produto ao carrinho
    cy.get('#homefeatured > .last-item-of-tablet-line.first-item-of-mobile-line > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click();//confirma produto ao carrinho
    cy.get('.continue > span').click();
    
})

  it('Conclui processo de compra', () => {
    cy.visit('http://automationpractice.com/index.php?controller=authentication&back=my-account');
    cy.get('.login').click();
    cy.get('#email').type('zoslley@pactosolucoes.com.br');
    cy.get('#passwd').type('123456');
    cy.get('#SubmitLogin > span').click();
    cy.get('.home').click();//adiciona produto ao carrinho
    cy.get('#homefeatured > .last-item-of-tablet-line.first-item-of-mobile-line > .product-container > .right-block > .button-container > .ajax_add_to_cart_button > span').click();//confirma produto ao carrinho
    cy.get('.button-medium > span').click(); //vai para carrinho
    cy.get('[title="View my shopping cart"]');//vai para pagamento
    cy.get('.cart_navigation > .button > span').click();
    cy.get('.cart_navigation > .button > span').click();
    cy.get('#cgv').click();
    cy.get('.cart_navigation > .button > span').click();
    cy.get('.bankwire').click();
    cy.get('#cart_navigation > .button > span').click();

})

  it('Consulta pedido Realizado', () => {
    cy.get('.button-exclusive').click();
    cy.get('.login').click();
    cy.get('#email').type('zoslley@pactosolucoes.com.br');
    cy.get('#passwd').type('123456');
    cy.get('#SubmitLogin > span').click();
    cy.get(':nth-child(1) > .myaccount-link-list > :nth-child(1) > a > span').click();
    
})

  expect(true).to.equal(true)
});