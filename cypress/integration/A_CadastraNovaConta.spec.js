/// <reference types="cypress" />

describe('Cadastra Nova Conta', () => {
  it('Cadastra Nova Conta', () => {

    cy.visit('http://automationpractice.com/index.php');
    cy.get('.login').click()
    cy.get('#email_create').type('zuuamarie@hotmail.com');
    cy.get('#SubmitCreate > span').click();
    cy.get('#id_gender1').click();
    cy.get('#customer_firstname').type('zoslley');
    cy.get('#customer_lastname').type('Adriel');
    cy.get('#passwd').type('123456');
    cy.get('#days').select('30');
    cy.get('#months').select('April');
    cy.get('#years').select('1983');
    cy.get('#company').type('Pacto Solucoes');
    cy.get('#address1').type('Rua, C200, N243');
    cy.get('#address1').type('Jardim America');
    cy.get('#city').type('Goiania')
    cy.get('#id_state').select('Florida')
    cy.get('#postcode').type('00000')
    //cy.get('#id_country').select('United states')
    cy.get('#other').type('Estou fazendo esse teste para adquirir mais conhecimento')
    cy.get('#phone').type('12345678901')
    cy.get('#phone_mobile').type('01234567890')
    cy.get('#alias').clear().type('zuuamarie@hotmail.com')
    cy.get('#submitAccount > span').click();

    expect(true).to.equal(true)
  })
})

